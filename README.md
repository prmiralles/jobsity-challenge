# Jobsity

This project is a challengue to deliver for a job application

## Installation
For depedencies npm was used, you can get it by installing Node(https://nodejs.org/en/download/)
```bash
npm install
```

## Compile SASS to CSS
To compile files, all the dev dependencies should have been downloaded in the previous step, now to generate the css files you must perform the following command in the terminal
```bash
npm run css
```
This will run the necessary commands to generate the file

Miralles, P.